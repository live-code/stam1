import { Component } from '@angular/core';
import { LogService } from './core/services/log.service';

@Component({
  selector: 'st-root',
  template: `
   
    <button routerLinkActive="active" routerLink="login">Login</button>
    <button routerLinkActive="active" routerLink="catalog">Catalog</button>
    <button routerLinkActive="active" routerLink="uikit">Uikit</button>
    
    <div class="container m-2">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'stam1';
}
