import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel/panel.component';
import { FormsModule } from '@angular/forms';
import { PanelModule } from './components/panel/panel.module';
import { CounterService } from './services/counter.service';
import { CardComponent } from './components/card.component';



@NgModule({
  declarations: [
  
    CardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PanelModule
  ],
  exports: [
    FormsModule,
    PanelModule,
    CardComponent
  ],
  providers: [
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        CounterService
      ]
    }
  }
}
