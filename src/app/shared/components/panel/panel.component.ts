import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PanelService } from './panel.service';

@Component({
  selector: 'st-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
   <div (click)="panelService.open = ! panelService.open">
     PANEL COMPONENT
     {{panelService.open}}
   </div>

   {{ render()}}
  `,
  providers: [
    PanelService
  ]
})
export class PanelComponent implements OnInit {

  constructor(public panelService: PanelService) { }

  ngOnInit(): void {
  }

  render() {
    console.log('render panel')
  }
}
