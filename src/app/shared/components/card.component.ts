import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
interface User {
  name: string;
}

@Component({
  selector: 'st-card',
  template: `
    <div class="card">
      <div class="card-header" [ngClass]="headerCls" (click)="opened = !opened">
        {{title}}
        <div class="pull-right" *ngIf="icon">
          <i [class]="'fa fa-' + icon" (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content select=".header"></ng-content>
      </div>
      
      <div class="card-footer">
        <ng-content select=".footer"></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string | undefined;
  @Input() icon: string | undefined;
  @Input() headerCls: string = ''
  @Input() url: string | undefined;
  @Input() actionType: 'openUrl' | 'openRoute' | undefined
  @Output() iconClick = new EventEmitter()
  opened = true;

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit()
  }
}
