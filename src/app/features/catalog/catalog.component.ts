import { Component, ViewEncapsulation } from '@angular/core';
import { CounterService } from '../../shared/services/counter.service';
import { LogService } from '../../core/services/log.service';

@Component({
  selector: 'st-catalog',
  // encapsulation: ViewEncapsulation.ShadowDom,
  template: `
    {{counterService.value}}
    <button class="btn btn-primary bg" (click)="counterService.inc()">+</button>

    <st-editor></st-editor>
    <st-editor></st-editor>
    <st-editor></st-editor>
    
  `,
  styles: [`
    .btn {
      
    }
    h1.bg { background-color: cyan}
    .bg { background-color: purple}
  `]
})
export class CatalogComponent {
  constructor(public counterService: CounterService, private logService: LogService) {}

}
