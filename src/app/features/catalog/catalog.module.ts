import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { CounterService } from '../../shared/services/counter.service';
import { EditorModule } from './modules/editor/editor.module';


@NgModule({
  declarations: [
    CatalogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    CatalogRoutingModule,
    EditorModule
  ],
  providers: [

  ]
})
export class CatalogModule { }
