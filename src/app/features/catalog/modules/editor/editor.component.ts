import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'st-editor',
  template: `
  
    <h1 class="bg">Editor</h1>
    <st-editor-one></st-editor-one>
    <st-editor-two></st-editor-two>
    <st-editor-three></st-editor-three>
  `,

})
export class EditorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
