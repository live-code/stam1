import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor.component';
import { EditorOneComponent } from './components/editor-one.component';
import { EditorTwoComponent } from './components/editor-two.component';
import { EditorThreeComponent } from './components/editor-three/editor-three.component';



@NgModule({
  declarations: [
    EditorComponent,
    EditorOneComponent,
    EditorTwoComponent,
    EditorThreeComponent
  ],
  exports: [
    EditorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class EditorModule { }
