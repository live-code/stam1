import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { LangService } from '../../shared/services/lang.service';
import { CounterService } from '../../shared/services/counter.service';
import { LogService } from '../../core/services/log.service';

@Component({
  selector: 'st-login',
  template: `
    <h1>Login</h1>
    <!--
    {{counterService.value}}
    <button (click)="counterService.inc()">+</button>
    -->
    <router-outlet></router-outlet>
    <hr>
    
    <button routerLink="registration" routerLinkActive="active">registration</button>
    <button routerLink="/login/lost" routerLinkActive="active">lost</button>
    <button routerLink="../login/signin" routerLinkActive="active">signin</button>
  `,
  styles: [`
  `]
})
export class LoginComponent implements OnInit {

  constructor(public counterService: CounterService, private logService: LogService) {
  }

  ngOnInit(): void {
  }

}
