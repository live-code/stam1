import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { RegistrationComponent } from './components/registration.component';
import { SigninComponent } from './components/signin.component';
import { LostpassComponent } from './components/lostpass.component';
import { SharedModule } from '../../shared/shared.module';
import { environment } from '../../../environments/environment';
import { LogService } from '../../core/services/log.service';
import { FakeLogService } from '../../core/services/fake-log.service';


@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    LostpassComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
        {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', loadChildren: () => import('./components/signin.module').then(m => m.SigninModule) },
          { path: 'registration', component: RegistrationComponent },
          { path: 'lost', component: LostpassComponent },
        ]
      },

    ])
  ],

})
export class LoginModule { }



export class Pippo {}
export const PI = 3.14
