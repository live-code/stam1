import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitRoutingModule } from './uikit-routing.module';
import { UikitComponent } from './uikit.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PanelModule } from '../../shared/components/panel/panel.module';
import { ThemeService } from '../../core/services/theme.service';


@NgModule({
  declarations: [
    UikitComponent,
  ],
  imports: [
    CommonModule,
    UikitRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    ThemeService
  ]
})
export class UikitModule { }
