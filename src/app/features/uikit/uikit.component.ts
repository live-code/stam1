import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'st-uikit',
  template: `
    <h1>Uikit</h1>
    
    <a [href]="url">visit it</a>
    
    <st-card title="profile"
             icon="linkedin" 
             headerCls="bg-dark text-white"
             (iconClick)="openUrl('http://www.google.com')"
    >
      <div class="header">
        <input type="text" ngModel>
        <button (click)="doSomething()">DO!</button>
      </div>
      
      <div class="footer">
        ..coopryigr....footer...
      </div>
    </st-card>
    
    
    <st-card title="profile" 
             icon="facebook" (iconClick)="visible = !visible">
     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aut consectetur cupiditate ducimus eum, ex inventore nesciunt numquam obcaecati porro, quaerat quidem sequi similique suscipit tempore temporibus ut vel voluptatibus.
    </st-card>
    
    <div *ngIf="visible">
      <st-panel></st-panel>
      <st-panel></st-panel>
      <st-panel></st-panel>
    </div>
    
    <div [ngClass]="'btn btn-primary'">xxxx</div>
    <div [ngClass]="['btn', 'btn-primary']">xxxx</div>
    <div class="btn" [ngClass]="getCls()">xxxx</div>
    
  `,
})
export class UikitComponent implements OnInit {
  visible = true;
  city = 'trieste'
  url = 'http://www.google.com';
  catId = 123

  getCls() {
    console.log('render')
    const list = [];

    if (true) {
      list.push('btn')
      list.push('btn-warning')
    }
    return list;
  }


  constructor(private themeService: ThemeService) {
    console.log(themeService)
  }

  ngOnInit(): void {
  }

  doSomething() {
    console.log('do something')
  }


  openUrl(url: string) {
    window.open(url)
  }

  getTitle() {
    console.log('render title')
  }
}
