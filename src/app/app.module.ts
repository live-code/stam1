import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloComponent } from './components/hello.component';
import { ThemeService } from './core/services/theme.service';
import { SharedModule } from './shared/shared.module';
import { LogService } from './core/services/log.service';
import { environment } from '../environments/environment';
import { FakeLogService } from './core/services/fake-log.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

export const APP_CONFIG = new InjectionToken<number>('app.config');


@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    HttpClientModule,
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: 1234
    },
    {
      provide: LogService,
      useFactory: (http: HttpClient, appConfig: number) => {
        return !environment.production ? new LogService(http, appConfig) : new FakeLogService()
      },
      deps: [HttpClient, APP_CONFIG]
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
