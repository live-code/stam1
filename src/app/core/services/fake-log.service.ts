import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FakeLogService {
  constructor() {
    console.log('fake log service')
  }
}
