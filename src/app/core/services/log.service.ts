import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG } from '../../app.module';

@Injectable()
export class LogService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) counter?: number) {
    console.log('log service', counter)
  }
  init() {

  }
}
